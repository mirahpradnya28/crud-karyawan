<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataKaryawanController extends Controller
{
    public function show(){
        return view('InsertKaryawan',['page' => 1]);
    }

    public function viewData(){
        $karyawan = DB::table('karyawan')-> get();
        return view('viewKaryawan', ['karyawan'=>$karyawan], ['page' => 2]);
    }

    public function simpanData(Request $request){

        DB::table('karyawan')->insert(
            [
                'nama_karyawan'=>$request->nama, 
                'no_karyawan'=>$request->nomor, 
                'no_telp_karyawan'=>$request->telepon, 
                'jabatan_karyawan'=>$request->jabatan , 
                'divisi_karyawan'=>$request->divisi, 
            ]
        );

        return redirect('/InsertKaryawan')-> with('status', 'Data Karyawan Berhasil Ditambahkan');
    }

    public function hapusData($id){
        DB::table('karyawan')->where('id',$id)->delete();

        return redirect('/viewKaryawan');
    }

    public function editData($id){
        $karyawan = DB::table('karyawan')-> where('id',$id)->get();
        return view('editKaryawan',['karyawan'=>$karyawan]);
    }

    public function updateData(Request $request){
        $karyawan= DB::table('karyawan')->where('id', $request->id)->update([
            'nama_karyawan'=>$request->nama, 
            'no_karyawan'=>$request->nomor, 
            'no_telp_karyawan'=>$request->telepon, 
            'jabatan_karyawan'=>$request->jabatan , 
            'divisi_karyawan'=>$request->divisi, 
        ]);

        return redirect ('/viewKaryawan')-> with('status', 'Data Karyawan Berhasil Diupdate');
    }
}
