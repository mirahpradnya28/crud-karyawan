<?php

use App\Http\Controllers\DataKaryawanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[DataKaryawanController::class,'show'])->name('InsertKaryawan');
Route::get('/InsertKaryawan',[DataKaryawanController::class,'show'])->name('InsertKaryawan');;
Route::post('/simpan',[DataKaryawanController::class,'simpanData']);
Route::get('/viewKaryawan',[App\Http\Controllers\DataKaryawanController::class,'viewData'])->name('viewKaryawan');;
Route::get('/hapusKaryawan/{id}',[DataKaryawanController::class,'hapusData']);
Route::get('/editKaryawan/{id}',[DataKaryawanController::class,'editData']);
Route::post('/update',[DataKaryawanController::class,'updateData']);
