@extends('master/app')
@section('konten')
      <!-- form_lebal -->
      <section>
         <div class="container">
            <div class="column">
               <div class="col-md-12">
                  <div class="form_book">
                    <div class="col-6">
                        @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                    </div>
                      <table align="center" style="width: 100%">
                          <tr>
                              <th>Nama</th>
                              <th>Nomor</th>
                              <th>No Telepon</th>
                              <th>Jabatan</th>
                              <th>Divisi</th>
                              <th colspan="2" >Action</th>
                          </tr>
                          @foreach ($karyawan as $k)
                          <tr>
                              <td>{{ $k->nama_karyawan}}</td>
                              <td>{{ $k->no_karyawan}}</td>
                              <td>{{ $k->no_telp_karyawan}}</td>
                              <td>{{ $k->jabatan_karyawan}}</td>
                              <td>{{ $k->divisi_karyawan}}</td>
                              <td><a href="/editKaryawan/{{ $k->id }}"><i class="gg-pen"></i> </a></td>
                              <td><a href="/hapusKaryawan/{{ $k->id }}" onclick="return confirm('Apakah anda yakin menghapus data ini?')"><i class="gg-trash"></i></a> </td> 
                          </tr>
                          @endforeach   
                      </table>
                     <br><br><br><br><br><br><br>
                  </div>
                </div>
            </div>
         </div>
      </section>
@endsection
