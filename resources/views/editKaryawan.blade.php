<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <title>CRUD DATA KARYAWAN</title>
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css')?>" type="text/css">
      <!-- style css -->
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css"> 
      <!-- Responsive-->
      <link rel="stylesheet" href="css/responsive.css">
      <link href='https://css.gg/trash.css' rel='stylesheet'>
      <link href='https://css.gg/pen.css' rel='stylesheet'>
      <link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css"> 
      <link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css')?>" type="text/css">
   </head>
   <!-- body -->
   <body class="main-layout">
      <section class="banner_main">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">
                     <div class="text-bg">
                           <h1>DATA KARYAWAN </h1>
                           <p>Silahkan Edit Data Karyawan</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
 <!-- form_lebal -->
 <section><div class="container">
      <div class="column">
         <div class="col-md-12">
            <div class="form_book">
                @foreach ($karyawan as $k)
               <form action="/update" method="post">
                  {{ csrf_field() }}
                  <div class="col order-5">
                      <input type="hidden" class="form-control" id="id" name="id" value="{{ $k->id }}">
                 </div>
                  <div class="col order-5">
                        <label for="nama" class="form-label">Nama Karyawan</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ $k->nama_karyawan }}">
                  </div>
                  <div class="col order-5">
                        <label for="nomor" class="form-label">Nomor Karyawan</label>
                        <input type="number" class="form-control" id="nomor" name="nomor" value="{{ $k->no_karyawan }}">
                  </div>
                  <div class="col order-5">
                        <label for="telepon" class="form-label">Nomor Telepon Karyawan</label>
                        <input type="number" class="form-control" id="telepon" name="telepon" value="{{ $k->no_telp_karyawan }}">
                  </div>
                  <div class="col order-5">
                        <label for="jabatan" class="form-label">Jabatan Karyawan</label>
                        <input type="text" class="form-control" id="jabatan" name="jabatan" value="{{ $k->jabatan_karyawan }}">
                  </div>
                  <div class="col order-5">
                        <label for="divisi" class="form-label">Divisi Karyawan</label>
                        <input type="text" class="form-control" id="divisi" name="divisi" value="{{ $k->divisi_karyawan }}">
                  </div>
                  <br>
                  <div class="col order-5">
                        <button class="btn btn-primary" type="submit">Edit</button>
                  </div>
            </form>
            @endforeach
            </div>
         </div>
      </div>
   </div></section>

   </body>
</html>