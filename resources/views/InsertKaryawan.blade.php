@extends('master/app')
@section('konten')
      <!-- form_lebal -->
      <section>
         <div class="container">
               <div class="col-md-12">
                  <div class="form_book">
                     <form action="/simpan" method="post">
                        {{ csrf_field() }}
                        <div class="col-6">
                              @if (session('status'))
                              <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  {{ session('status') }}
                              </div>
                              @endif
                          </div>
                        <div class="col order-5">
                              <label for="nama" class="form-label">Nama Karyawan</label>
                              <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="col order-5">
                              <label for="nomor" class="form-label">Nomor Karyawan</label>
                              <input type="number" class="form-control" id="nomor" name="nomor">
                        </div>
                        <div class="col order-5">
                              <label for="telepon" class="form-label">Nomor Telepon Karyawan</label>
                              <input type="number" class="form-control" id="telepon" name="telepon">
                        </div>
                        <div class="col order-5">
                              <label for="jabatan" class="form-label">Jabatan Karyawan</label>
                              <input type="text" class="form-control" id="jabatan" name="jabatan">
                        </div>
                        <div class="col order-5">
                              <label for="divisi" class="form-label">Divisi Karyawan</label>
                              <input type="text" class="form-control" id="divisi" name="divisi">
                        </div>
                        <br>
                        <div class="col order-5">
                              <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                  </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
@endsection