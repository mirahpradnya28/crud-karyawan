<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <title>CRUD DATA KARYAWAN</title>
      <!-- bootstrap css -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css')?>" type="text/css">
      <!-- style css -->
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css"> 
      <!-- Responsive-->
      <link rel="stylesheet" href="css/responsive.css">
      <link href='https://css.gg/trash.css' rel='stylesheet'>
      <link href='https://css.gg/pen.css' rel='stylesheet'>
      <link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css"> 
      <link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css')?>" type="text/css">
   </head>
   @if ($page==1)
    <body class="InsertKaryawan">
        @elseif ($page==2)

        <body class="viewKaryawan">
           @endif

   <!-- body -->
   <body class="main-layout">
      <div class="header">
         <div class="container">
            <div class="row">
               <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                  <nav class="navigation navbar navbar-expand-md navbar-dark ">
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                     </button>
                     <div class="collapse navbar-collapse" id="navbarsExample04">
                        <ul class="navbar-nav mr-auto">
                           @if ($page==1)
                              <li class="nav-item active">
                           @else
                              <li class="nav-item">
                           @endif
                              <a class="nav-link" href="{{route('InsertKaryawan')}}">Tambah Data</a>
                           </li>
                           @if ($page==2)
                              <li class="nav-item active">
                           @else
                              <li class="nav-item">
                           @endif
                              <a class="nav-link" href="{{route('viewKaryawan')}}">Lihat Data</a>
                           </li>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
      </div>
   </header>

      <section class="banner_main">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="text-bg">
                        <h1>DATA KARYAWAN </h1>
                        <p>Silahkan masukkan, edit, hapus dan liat seluruh data karyawan</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      @yield('konten')
   </body>
</html>